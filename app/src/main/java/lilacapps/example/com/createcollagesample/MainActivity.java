package lilacapps.example.com.createcollagesample;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lopei.collageview.CollageView;

import java.sql.Array;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button show_collage_btn = findViewById(R.id.btn_show_collage);
        show_collage_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int[] list_of_urls = new int[6];
                list_of_urls[1] = R.drawable.draw;
                list_of_urls[0] = R.drawable.capture1;
                list_of_urls[3] = R.drawable.new1;
                list_of_urls[2] = R.drawable.new2;
                list_of_urls[4] = R.drawable.ne4;
                list_of_urls[5] = R.drawable.new3;
                show_collage_btn.setVisibility(View.GONE);
                CollageView collageView = findViewById(R.id.collageView);
                collageView.setVisibility(View.VISIBLE);
                collageView
//                        .photoMargin(1)
//                        .photoPadding(4)
                        .backgroundColor(Color.WHITE)
                        .photoFrameColor(Color.BLUE)
//                        .useFirstAsHeader(true) // makes first photo fit device widtdh and use full line
//                        .defaultPhotosForLine(6) // sets default photos number for line of photos (can be changed by program at runtime)
                        .useCards(true) // adds cardview backgrounds to all photos
                        .maxWidth(100) // will resize images if their side is bigger than 100
//                        .placeHolder(R.drawable.draw) //adds placeholder resource
//                        .headerForm(CollageView.ImageForm.IMAGE_FORM_SQUARE) // sets form of image for header (if useFirstAsHeader == true)
//                        .photosForm(CollageView.ImageForm.IMAGE_FORM_HALF_HEIGHT) //sets form of image for other photos
                        .loadPhotos(list_of_urls);
            }
        });
    }
}
